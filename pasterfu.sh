#!/bin/bash

source ~/.pasterfu/pasterfu.conf


# asking timestamp
function ask_stamp () {
	tformat=""
	stamp=""
	stamp=$(zenity --entry \
		--title "Timestamp" \
		--width=$width --height=$height \
		--text "Link - $clip\n\nTimestamp formats [XmYs] || [Xm] || [Y(s)]\nExample: 1m1s or 61")
}

# check link type
# get the video URI from the link
# paste video URI, timestamp and link into clipboard
function y_choose () {
	ask_stamp

	if [ -z "${clip##*youtube.com/watch?v=*}" ]; then
		video=$(echo $clip |grep -Po '(?<=v=).*')
		clip="https://youtu.be/$video?t=$stamp"

	elif [ -z "${clip##*youtu.be/*}" ]; then
		video=$(echo $clip |cut -f1 -d"?")
		clip="$video?t=$stamp"
	fi

	# write changes to clipboard
	echo "$clip" |xclip -selection clipboard -i
}

# check if link has watch?v=
# does link already have time attribute &t= or ?t= and remove it
# write new link with timestamp to clipboard
function h_choose () {
	ask_stamp

	if [ -z "${clip##*watch?v=*}" ]; then
		[[ -z "${clip##*&t=*}" ]] && clip=$(echo "$clip" |cut -f1 -d"&")
		tformat="&t="
	else
		[[ -z "${clip##*?t=*}" ]] && clip=$(echo "$clip" |cut -f1 -d"?")
		tformat="?t="
	 fi

	# write changes to clipboard
	clip="$clip$tformat$stamp"
	echo "$clip" |xclip -selection clipboard -i
}

# add Youtube Stamp or Hooktube Stamp to $programs (starters)
# y_choose | h_hoose -runnable functions if chosen with zenity
function stamp () {
	if [ -z "${clip##*youtube.com/watch?v=*}" ] || [ -z "${clip##*youtu.be/*}" ]; then
		programs=$(
			echo "Youtube Stamp"
			echo "y_choose"
			echo "$programs")
	elif [ -z "${clip##*hooktube.com/*}" ]; then
		programs=$(
			echo "Hooktube Stamp"
			echo "h_choose"
			echo "$programs")
	fi
}

function hooktube () {
	# change link to hooktube
	if [ -z "${clip##*youtube.com/watch?v=*}" ]; then
		clip="${clip//youtube/hooktube}"
		echo "$clip" |xclip -selection clipboard -i
	elif [ -z "${clip##*youtu.be/*}" ]; then
		clip="${clip//youtu.be/hooktube.com}"
		echo "$clip" |xclip -selection clipboard -i
	fi  
}

function hooktubefy () {
	# if link is youtube link
	# change youtube to hooktube
	if [ -z "${clip##*youtube.com/watch?v=*}" ] || [ -z "${clip##*youtu.be/*}" ]; then
		programs=$(
			echo "Hooktubefy"
			echo "hooktube"
			echo "$programs")
	fi
}

function clearclip () {
	# clears clipboard if zenity has run successfully
	# (zenity output variable not empty)
	if [[ -n $paster ]]; then
		echo "" |xclip -selection clipboard -i
	fi
}


# continue if chosen pwmanager is not on or pwsec is false
# continue if clipboard starts with http:// or https://
if [[ $pwsec == false ]] || [[ $(wmctrl -lx |grep -Po '(?<=\.).*(?=  )' |egrep "$pwman" 2>&1 |wc -l) -eq 0 ]]; then
    if [[ $(xclip -selection clipboard -o) == *"http://"* ]] || [[ $(xclip -selection clipboard -o) == *"https://"* ]]; then
        clip=$(xclip -selection clipboard -o)

		# run program functin from config to get list
		program

		# change youtube links to hooktube if hookall is on
		$hookall == true && htube=false && hooktube

		# run stamp function if ystamp is on
		$ystamp == true && stamp

		# run hooktube function if htube is on
		$htube == true && hooktubefy

		# Program list for Zenity
		paster=""
		paster=$(echo "$programs" |zenity --list \
			--title "Choose Starter" \
			--width=$width --height=$height \
			--print-column=2 \
			--hide-column=2 \
			--column "Program" \
			--column "Command" \
			--text="Link - $clip")

		# Run chosen option from zenity
		$paster

		# run clearclip function if enabled
		$clclip == true && clearclip

	elif [[ $noerror == false ]]; then
		zenity --width=$width --height=$height --error --text "No link found in clipboard."
	fi
elif [[ $noerror == false ]]; then
	zenity --width=$width --height=$height --error --text "Function halted because of a pwmanager."
fi
