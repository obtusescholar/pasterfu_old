# I am currently redesigning and rewrinting the whole script from scratch.

## pasterfu

Bash script with Zenity GUI that pastes links from clipboard to user defined programs.

##### Dependencies
*  zenity
*  xclip

##### Optional dependencies
*  wmctrl (to check if password manager is on)

## Installing

1.  Install dependencies zenity and xclip (Optionally wmctrl)
2.  Make directory `mkdir ~/.pasterfu`
3.  Copy pasterfu.sh and pasterfu.conf into ~/.pasterfu/.
4.  Make pasterfu.sh executable `chmod +x ~/.pasterfu/pasterfu.sh`
5.  Deside how to invoke the script. (For examble keybinding)

## Screenshots

##### Pasterfu

The main window.

![pasterfu](/Screenshot/pasterfu.png)

##### Zenity Search

Mouseless functionality with Zenity Search. Just start writing and press Enter.

![pasterfu](/Screenshot/zenity_search.png)

##### Youtube Stamp

Youtube Timestamping with optional module.

![pasterfu](/Screenshot/youtubestamp.png)